<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Models\User;

class EventController extends Controller
{
    public function index()
    {

        $search = request('search');

        if($search){
            $events = Event::where([
                ['title', 'like', '%'.$search.'%']
            ])->get();
        } else {
            $events = Event::all();
        }
       

        return view('welcome', [
            'events' => $events,
            'search' => $search
        ]);
    }

    public function create()
    {
        return view('events.create');
    }

    public function store(Request $request)
    {

        $event = new Event;

        $event->title = $request->title;
        $event->date = $request->date;
        $event->time = $request->time;
        $event->city = $request->city;
        $event->private = $request->private;
        $event->description = $request->description;
        $event->items = json_encode($request->items);

        //Image validation to Upload
        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $requestImage = $request->image;
            $extension = $requestImage->extension();
            $imageName = md5($requestImage->getClientOriginalName() . strtotime("now")) . "." . $extension;

            $request->image->move(public_path('img/events'),  $imageName);

            $event->image = $imageName;
        }

        //preenchendo user_id automatically

        $user =  auth() -> user();
        $event->user_id = $user->id;

        //save
        $event->save();

        return redirect()->route('welcome.events')->with('msg', 'Evento criado com sucesso');
    }

    public function show($id)
    {
        $event = Event::findOrFail($id);

        $user = auth() -> user();
        $hasUserJoined = false;

        if($user){
            $userEvents = $user->eventsAsParticipants->toArray();
            foreach($userEvents as $userEvent){
                if($userEvent['id'] == $id){
                    $hasUserJoined = true;
                }
            }
        }

        $eventOwner =  User::where('id', $event->user_id)->first()->toArray();

        return view('events.show', [
            'event' => $event,
            'eventOwner' => $eventOwner,
            'hasUserJoined' => $hasUserJoined
        ]);
    }

    public function dashboard(){
        $user = auth()->user();
        $events = $user->events;

        $eventsAsParticipants = $user->eventsAsParticipants;
        
        return view('events.dashboard', [
            'events' => $events,
            'eventsAsParticipants' => $eventsAsParticipants
        ]); 
    }

    public function destroy($id){
        Event::findOrFail($id)->delete();
        return redirect()->route('dashboard.events')->with('msg', 'Evento deletado com sucesso');
    }

    public function edit($id){

        $user = auth()->user();
        $event = Event::findOrFail($id);

        if($user->id != $event->user_id){
            return redirect()->route('dashboard.events');
        }
        return view('events.edit', ['event' => $event]);
    }

    public function update($id, Request $request){
        Event::findOrFail($request->id)
        ->update($request->all());

        return redirect()->route('dashboard.events')->with('msg', 'Evento editado com sucesso');
    }

    public function joinEvent($id){
        $user = auth()->user();
        $user->eventsAsParticipants()->attach($id);
        $event = Event::findOrFail($id);

        return redirect()->route('dashboard.events')->with('msg', 'Sua presença está confirmada no evento'. $event->title);
    }

    public function leaveEvent($id){
        $user = auth()->user();
        $user->eventsAsParticipants()->detach($id);
        $event = Event::findOrFail($id);

        return redirect()->route('dashboard.events')->with('msg', 'Candidatura encerrada em: '. $event->title);
    }
}
