<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $table = 'events';
    protected $fillable = ['title', 'description', 'city', 'private', 'image','items', 'date', 'time'];
    
    protected $casts = [
        'items' => 'array',
    ];

    protected $dates = ['date'];

    protected $time = ['time'];

    //relation one to many
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
}