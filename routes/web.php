<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EventController::class, 'index'])->name('welcome.events');
Route::post('/events/store', [EventController::class, 'store'])->name('store.events');
Route::get('/events/contact', [EventController::class, 'contact'])->name('contact.events');
Route::get('/events/create', [EventController::class, 'create'])->middleware('auth')->name('create.events');
Route::get('/events/edit{id}', [EventController::class, 'edit'])->middleware('auth')->name('edit.events');
Route::put('/events/update/{id}', [EventController::class, 'update'])->middleware('auth')->name('update.events');
Route::delete('/events/{id}', [EventController::class, 'destroy'])->middleware('auth')->name('destroy.events');
Route::get('/events/{id}', [EventController::class, 'show'])->name('show.events');


Route::get('/dashboard', [EventController::class, 'dashboard'])->middleware('auth')->name('dashboard.events');

Route::post('/events/join/{id}', [EventController::class, 'joinEvent'])->middleware('auth')->name('join.events');
Route::delete('/events/leave/{id}', [EventController::class, 'leaveEvent'])->middleware('auth')->name('leave.events');

