<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <!--Google Fonts-->
        <link href="https://fonts.googleapis.com/css2?family=Roboto" rel="stylesheet">
        <!--Bootstrap 5.0 da Aplicação-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <!--JS da Aplicação-->
        <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
        <!--Css da Aplicação-->
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
    </head>
    <body>
        <header>
          <nav class="navbar navbar-expand-lg navbar-light">
            <div class="collapse navbar-collapse" id="navbar">
              <a href="/" class="navbar-brand">
                <img src="{{asset('img/logo-hdc.png')}}" alt="HDC Events">
              </a>
              <ul class="navbar-nav">
                <li class=" nav nav-item">
                  <a href="{{route('welcome.events')}}" class="nav-link">Eventos </a>
                </li>
                @auth
                <li class="nav-item">
                  <a href="{{route('create.events')}}" class="nav-link">Criar Eventos</a>
                </li>
                <li class="nav-item">
                  <a href="{{route('dashboard.events')}}" class="nav-link">Meus Eventos</a>
                </li>
                <li class="nav-item">
                  <form action="/logout" method="post">
                    @csrf
                    <a href="/logout" 
                      class="nav-link" 
                      onclick="event.preventDefault();
                      this.closest('form').submit();">
                      Sair
                    </a>
                  </form>
                </li>
                @endauth
                @guest
                  <li class="nav-item">
                    <a href="/login"" class="nav-link">Entrar</a>
                  </li>
                  <li class="nav-item">
                    <a href="/register" class="nav-link">Cadastrar</a>
                  </li>
                @endguest
              </ul>
            </div>
          </nav>
        </header>
        <main>
          <div class="container-fluid">           
            <div class="row">
              @if (session('msg'))
                  <p class="msg">{{session('msg')}}</p>
              @endif
              
              @yield('conteudo')
            </div>
          </div>
        </main>
        <footer>
          <p>HDC Events &copy; 2022</p>
        </footer>
      <!--JavaScript Bundle BOOTSTRAP-->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
      <!--ION icons CDN-->
      <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
      <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
  </body>
</html>
