@extends('layout.main')
@section('title', 'HDC - criar evento')
@section('conteudo')
  <div id="event-create-container" class="col-md-6 offset-md-3">
    <h1>Crie o seu evento</h1>
    <form action="{{route('store.events')}}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label for="image">Imagem do evento</label>
        <input type="file" name="image" id="image" class="form-control-file" required>
      </div>
      <div class="form-group">
        <label for="title">Evento</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Nome do Evento" required>
      </div>
      <div class="form-group">
        <label for="date">Data do evento</label>
        <input type="date" class="form-control" id="date" name="date" required>
      </div>
      <div class="form-group">
        <label for="time">Horário do evento</label>
        <input type="time" class="form-control" id="time" name="time" required>
      </div>
      <div class="form-group">
        <label for="title">Cidade</label>
        <input type="text" class="form-control" id="city" name="city" placeholder="maracanaú" required>
      </div>
      <div class="form-group">
        <label for="private">O evento é privado?</label>
        <select class="form-control" name="private" id="private" required>
          <option value="0">Não</option>
          <option value="1">Sim</option>
        </select>
      </div>
      <div class="form-group"> 
        <label for="description">Descrição</label>
        <textarea name="description" id="description" class="form-control" placeholder="Explicar o evento" required></textarea>
      </div>
      <div class="form-group">
        <label for="title">Infraestrutura do Evento</label>
        <div class="form-group">
          <div class="form-group">
            <input type="checkbox" name="items[]" value="cadeira"> Cadeiras
          </div>
          <div class="form-group">
            <input type="checkbox" name="items[]" value="palco"> Palco
          </div>
          <div class="form-group">
            <input type="checkbox" name="items[]" value="openbar"> Openbar
          </div>
          <div class="form-group">
            <input type="checkbox" name="items[]" value="openfood"> Openfood
          </div>
          <div class="form-group">
            <input type="checkbox" name="items[]" value="brindes"> Brindes
          </div>
        </div>
      </div>
      <input type="submit" class="btn btn-primary" value="Criar Evento">
    </form>
  </div>
@endsection
