@extends('layout.main')
@section('title', 'Dashboard')
@section('conteudo')
<div class="col-md-10 offset-md-1 dashboard-title-container">
    <h1>Meus Eventos</h1>
</div>
<div class="col-md-10 offset-md-1 dashboard-events-container">
    @if(count($events) > 0)
    <table class="table">
        <thead>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Participantes</th>
            <th scope="col">Ações</th>
        </thead>
        <tbody>
            @foreach ($events as $event)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>
                        <a href="/events/{{$event->id}}">{{ $event->title}}</a>
                    </td>
                    <td>{{count($event->users)}}</td>
                    <td>
                        <a class="btn btn-info edit-btn" href="{{route('edit.events', $event->id)}}">
                            <ion-icon name="create-outline"></ion-icon>
                            Editar
                        </a>
                        <form action="{{route('destroy.events', $event->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger delete-btn" type="submit">
                                <ion-icon name="trash-outline"></ion-icon>
                                Deletar
                            </button>
                        </form>
                    </td>  
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p>Você ainda não cadastrou eventos. <a href="{{route('create.events')}}">Criar Evento</a></p>
    @endif
</div>
<div class="col-md-10 offset-md-1 dashboard-title-container">
    <h1>Eventos que estou participando</h1>
</div>
<div class="col-md-10 offset-md-1 dashboard-events-container">
    @if (count($eventsAsParticipants) > 0)
        <table class="table">
            <thead>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
                <th scope="col">Participantes</th>
                <th scope="col">Ações</th>
            </thead>
            <tbody>
                @foreach ($eventsAsParticipants as $event)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>
                            <a href="/events/{{$event->id}}">{{ $event->title}}</a>
                        </td>
                        <td>{{count($event->users)}}</td>
                        <td>
                            <form action="{{route('leave.events', $event->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger delete-btn" type="submit">
                                    <ion-icon name="trash-outline"></ion-icon>
                                    Sair do evento
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>Você ainda não está participando de nenhum evento, <a href="{{route('welcome.events')}}">Ver eventos disponíveis</a></p>
    @endif
</div>
@endsection
